<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Main',
    'Store Hours'
);
$TCA['tt_content']['types']['list']['subtypes_addlist']['storehours_main'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('storehours_main', 'FILE:EXT:'.$_EXTKEY.'/Configuration/FlexForms/Main.xml');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['storehours_main'] = 'layout,select_key,recursive';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Store Hours');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_storehours_domain_model_hour', 'EXT:store_hours/Resources/Private/Language/locallang_csh_tx_storehours_domain_model_hour.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_storehours_domain_model_hour');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_storehours_domain_model_store', 'EXT:store_hours/Resources/Private/Language/locallang_csh_tx_storehours_domain_model_store.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_storehours_domain_model_store');
