<?php
namespace GuteBotschafter\StoreHours\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Morton Jonuschat <mj@gute-botschafter.de>, Gute Botschafter GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Store
 */
class Store extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * openingHours
     *
     * @var string
     * @validate NotEmpty
     */
    protected $openingHours = '';

    /**
     * nationalHolidays
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $nationalHolidays = 0;

    /**
     * regionalHolidays
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $regionalHolidays = 0;

    /**
     * customHolidays
     *
     * @var string
     * @validate NotEmpty
     */
    protected $customHolidays = '';

    /**
     * hours
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GuteBotschafter\StoreHours\Domain\Model\Hour>
     * @cascade remove
     */
    protected $hours = null;

    /**
     * hours
     *
     * @var array
     */
    protected $extraHours = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->hours = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param  string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the openingHours
     *
     * @return string $openingHours
     */
    public function getOpeningHours()
    {
        return $this->openingHours;
    }

    /**
     * Sets the openingHours
     *
     * @param  string $openingHours
     * @return void
     */
    public function setOpeningHours($openingHours)
    {
        $this->openingHours = $openingHours;
    }

    /**
     * Returns the nationalHolidays
     *
     * @return integer $nationalHolidays
     */
    public function getNationalHolidays()
    {
        return $this->nationalHolidays;
    }

    /**
     * Sets the nationalHolidays
     *
     * @param  integer $nationalHolidays
     * @return void
     */
    public function setNationalHolidays($nationalHolidays)
    {
        $this->nationalHolidays = $nationalHolidays;
    }

    /**
     * Returns the regionalHolidays
     *
     * @return integer $regionalHolidays
     */
    public function getRegionalHolidays()
    {
        return $this->regionalHolidays;
    }

    /**
     * Sets the regionalHolidays
     *
     * @param  integer $regionalHolidays
     * @return void
     */
    public function setRegionalHolidays($regionalHolidays)
    {
        $this->regionalHolidays = $regionalHolidays;
    }

    /**
     * Returns the customHolidays
     *
     * @return string $customHolidays
     */
    public function getCustomHolidays()
    {
        return array_unique(array_filter(preg_split('#\R#', $this->customHolidays)));
    }

    /**
     * Sets the customHolidays
     *
     * @param  string $customHolidays
     * @return void
     */
    public function setCustomHolidays($customHolidays)
    {
        $this->customHolidays = $customHolidays;
    }

    /**
     * Adds a Hour
     *
     * @param  \GuteBotschafter\StoreHours\Domain\Model\Hour $hour
     * @return void
     */
    public function addHour(\GuteBotschafter\StoreHours\Domain\Model\Hour $hour)
    {
        $this->hours->attach($hour);
    }

    /**
     * Removes a Hour
     *
     * @param  \GuteBotschafter\StoreHours\Domain\Model\Hour $hourToRemove The Hour to be removed
     * @return void
     */
    public function removeHour(\GuteBotschafter\StoreHours\Domain\Model\Hour $hourToRemove)
    {
        $this->hours->detach($hourToRemove);
    }

    /**
     * Returns the hours
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GuteBotschafter\StoreHours\Domain\Model\Hour> $hours
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Returns the non-regular hours
     *
     * @return array $extraHours
     */
    public function getExtraHours()
    {
        if ($this->extraHours === null) {
            $this->extraHours = array_filter(
            $this->getHours()->toArray(),
            function ($hour) { return !$hour->getRegularHours(); }
          );
        }

        return $this->extraHours;
    }

    /**
     * Returns the regular hours
     *
     * @return array $regularHours
     */
    public function getRegularHours()
    {
        if ($this->regularHours === null) {
            $this->regularHours = array_filter(
            $this->getHours()->toArray(),
            function ($hour) { return $hour->getRegularHours(); }
          );
        }

        return $this->regularHours;
    }

    /**
     * Sets the hours
     *
     * @param  \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GuteBotschafter\StoreHours\Domain\Model\Hour> $hours
     * @return void
     */
    public function setHours(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $hours)
    {
        $this->hours = $hours;
    }

    /**
     * Check if the store is open
     *
     * @return boolean
     */
    public function isOpen()
    {
        if (null !== ($extraHour = $this->getExtraHoursForToday())) {
            return $extraHour->isOpen();
        }
        if ($this->isHoliday()) {
            return false;
        }
        if ($this->isOpenRegularHours()) {
            return true;
        }

        return false;
    }

    /**
     * Return special operating hours for today
     *
     * @return mixed \GuteBotschafter\StoreHours\Domain\Model\Hour|null
     */
    protected function getExtraHoursForToday()
    {
        foreach($this->getExtraHours() as $extraHour) {
            if($this->isToday($extraHour->getExtraDay())) {
                return $extraHour;
            }
        }
        return null;
    }

    /**
     * Check if store is open due to special openining times
     *
     * @return boolean
     */
    protected function isOpenExtraHours()
    {
        foreach ($this->getExtraHours() as $extraHour) {
            return 0 !== count(array_filter(
          $this->getExtraHours(),
          function ($hour) { return $hour->isOpen(); }
        ));
        }

        return false;
    }

    /**
     * Check if store is open due to regular openining times
     *
     * @return boolean
     */
    protected function isOpenRegularHours()
    {
        foreach ($this->getRegularHours() as $regularHour) {
            return 0 !== count(array_filter(
          $this->getRegularHours(),
          function ($hour) { return $hour->isOpen(); }
        ));
        }
    }

    /**
     * Check if today is an holiday day
     *
     * @return boolean
     */
    protected function isHoliday()
    {
        return $this->isNationalHoliday() || $this->isRegionalHoliday() || $this->isCustomHoliday();
    }

    /**
     * Check if today is an enabled national holiday
     *
     * @return boolean
     */
    protected function isNationalHoliday()
    {
        $daysFromEaster = $this->getDaysFromEaster();

      # New Year's day
      if ($this->nationalHolidays&1 && $this->isToday(new \DateTime('1st january'))) {
          return true;
      }

      # Good friday
      if ($this->nationalHolidays&2 && $daysFromEaster === -2) {
          return true;
      }

      # Easter monday
      if ($this->nationalHolidays&4 && $daysFromEaster === 1) {
          return true;
      }

      # Labor Day
      if ($this->nationalHolidays&8 && $this->isToday(new \DateTime('1st may'))) {
          return true;
      }

      # Ascension Day
      if ($this->nationalHolidays&16 && $daysFromEaster === 39) {
          return true;
      }

      # Whitmonday
      if ($this->nationalHolidays&32 && $daysFromEaster === 50) {
          return true;
      }

      # German unification day
      if ($this->nationalHolidays&64 && $this->isToday(new \DateTime('3rd october'))) {
          return true;
      }

      # Christmas Day
      if ($this->nationalHolidays&128 && $this->isToday(new \DateTime('25th december'))) {
          return true;
      }

      # Day after Christmas Day
      if ($this->nationalHolidays&256 && $this->isToday(new \DateTime('26th december'))) {
          return true;
      }

        return false;
    }

    /**
     * Check if today is an enabled regional holiday
     *
     * @return boolean
     */
    protected function isRegionalHoliday()
    {
        $daysFromEaster = $this->getDaysFromEaster();

      # Epiphany
      if ($this->regionalHolidays&1 && $this->isToday(new \DateTime('6th january'))) {
          return true;
      }

      # Holy Thursday
      if ($this->regionalHolidays&2 && $daysFromEaster === -3) {
          return true;
      }

      # Easter sunday
      if ($this->regionalHolidays&4 && $daysFromEaster === 0) {
          return true;
      }

      # Whitsunday
      if ($this->regionalHolidays&8 && $daysFromEaster === 49) {
          return true;
      }

      # Corpus Christi
      if ($this->regionalHolidays&16 && $daysFromEaster === 60) {
          return true;
      }

      # Augsburger Friedensfest
      if ($this->regionalHolidays&32 && $this->isToday(new \DateTime('8th august'))) {
          return true;
      }

      # Assumption Day
      if ($this->regionalHolidays&64 && $this->isToday(new \DateTime('15th august'))) {
          return true;
      }

      # Reformation Day
      if ($this->regionalHolidays&128 && $this->isToday(new \DateTime('1st october'))) {
          return true;
      }

      # All Hallows
      if ($this->regionalHolidays&256 && $this->isToday(new \DateTime('1st november'))) {
          return true;
      }

      # Day of Prayer and Repentance
      if ($this->regionalHolidays&512 && $this->isToday(new \DateTime('23rd november last wednesday'))) {
          return true;
      }

        return false;
    }

    /**
     * Check if today is an custom holiday
     *
     * @return boolean
     */
    protected function isCustomHoliday()
    {
        foreach ($this->getCustomHolidays() as $customHoliday) {
            try {
                if ($this->isToday(new \DateTime($customHoliday))) {
                    return true;
                }
            } catch (\Exception $e) {
            }
        }

        return false;
    }

    /**
     * Compare current date to given date
     *
     * @param boolean
     */
    protected function isToday(\DateTime $date)
    {
        return $this->getToday()->diff($date)->format('%a') == 0;
    }

    /**
     * Get current date/time
     *
     * @return \DateTime $date
     */
    protected function getToday()
    {
        return new \DateTime();
    }

    /**
     * return the days until/after easter
     *
     * @return integer
     */
    protected function getDaysFromEaster()
    {
        $easterSunday = \DateTimeImmutable::createFromFormat('U', easter_date($this->getToday()->format('Y')));

        return (int) $easterSunday->diff($this->getToday())->format('%R%a');
    }
}
