<?php
namespace GuteBotschafter\StoreHours\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Morton Jonuschat <mj@gute-botschafter.de>, Gute Botschafter GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Hour
 */
class Hour extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * regularHours
     *
     * @var boolean
     */
    protected $regularHours = true;

    /**
     * days
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $days = 0;

    /**
     * extraDay
     *
     * @var \DateTime
     */
    protected $extraDay = null;

    /**
     * morningOpenTime
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $morningOpenTime = 0;

    /**
     * morningCloseTime
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $morningCloseTime = 0;

    /**
     * afternoonOpenTime
     *
     * @var integer
     */
    protected $afternoonOpenTime = 0;

    /**
     * afternoonCloseTime
     *
     * @var integer
     */
    protected $afternoonCloseTime = 0;

    /**
     * Returns the regularHours
     *
     * @return boolean $regularHours
     */
    public function getRegularHours()
    {
        return $this->regularHours;
    }

    /**
     * Sets the regularHours
     *
     * @param  boolean $regularHours
     * @return void
     */
    public function setRegularHours($regularHours)
    {
        $this->regularHours = $regularHours;
    }

    /**
     * Returns the boolean state of regularHours
     *
     * @return boolean
     */
    public function isRegularHours()
    {
        return $this->regularHours;
    }

    /**
     * Is the store open using these opening hours
     *
     * @return boolean
     */
    public function isOpen()
    {
        if (!$this->isOpenAtThisTime()) {
            return false;
        }

        if ($this->isRegularHours() && $this->isOpenThisDayOfWeek()) {
            return true;
        }

        if (!$this->isRegularHours() && $this->isOpenAtThisDate()) {
            return true;
        }

        return false;
    }

    /**
     * Returns the days
     *
     * @return integer $days
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Sets the days
     *
     * @param  integer $days
     * @return void
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * Returns the extraDay
     *
     * @return \DateTime $extraDay
     */
    public function getExtraDay()
    {
        return $this->extraDay;
    }

    /**
     * Sets the extraDay
     *
     * @param  \DateTime $extraDay
     * @return void
     */
    public function setExtraDay(\DateTime $extraDay)
    {
        $this->extraDay = $extraDay;
    }

    /**
     * Returns the morningOpenTime
     *
     * @return integer $morningOpenTime
     */
    public function getMorningOpenTime()
    {
        return $this->morningOpenTime;
    }

    /**
     * Sets the morningOpenTime
     *
     * @param  integer $morningOpenTime
     * @return void
     */
    public function setMorningOpenTime($morningOpenTime)
    {
        $this->morningOpenTime = $morningOpenTime;
    }

    /**
     * Returns the morningCloseTime
     *
     * @return integer $morningCloseTime
     */
    public function getMorningCloseTime()
    {
        return $this->morningCloseTime;
    }

    /**
     * Sets the morningCloseTime
     *
     * @param  integer $morningCloseTime
     * @return void
     */
    public function setMorningCloseTime($morningCloseTime)
    {
        $this->morningCloseTime = $morningCloseTime;
    }

    /**
     * Returns the afternoonOpenTime
     *
     * @return integer $afternoonOpenTime
     */
    public function getAfternoonOpenTime()
    {
        return $this->afternoonOpenTime;
    }

    /**
     * Sets the afternoonOpenTime
     *
     * @param  integer $afternoonOpenTime
     * @return void
     */
    public function setAfternoonOpenTime($afternoonOpenTime)
    {
        $this->afternoonOpenTime = $afternoonOpenTime;
    }

    /**
     * Returns the afternoonCloseTime
     *
     * @return integer $afternoonCloseTime
     */
    public function getAfternoonCloseTime()
    {
        return $this->afternoonCloseTime;
    }

    /**
     * Sets the afternoonCloseTime
     *
     * @param  integer $afternoonCloseTime
     * @return void
     */
    public function setAfternoonCloseTime($afternoonCloseTime)
    {
        $this->afternoonCloseTime = $afternoonCloseTime;
    }

    /**
     * Returns the midnight of the current date
     *
     * @return \DateTime
     */
    protected function getMidnight()
    {
        return $this->getCurrentTime()->setTime(0, 0, 0);
    }

    /**
     * Returns the midnight of the extra day
     *
     * @return \DateTime
     */
    protected function getMidnightExtraDay()
    {
        if ($this->getExtraDay() === null) {
            return false;
        }
        $midnightExtraDay = clone($this->getExtraDay());

        return $midnightExtraDay->setTime(0, 0, 0);
    }

    /**
     * Returns the current day of week (0 = Monday, 6 = Sunday)
     *
     * @return integer
     */
    protected function getDayOfWeek()
    {
        return (int) $this->getCurrentTime()->format('N') - 1;
    }

    /**
     * Returns the current time
     *
     * @return \DateTime
     */
    protected function getCurrentTime()
    {
        return new \DateTimeImmutable();
    }

    /**
     * Returns the seconds since midnight for a timestamp
     *
     * @param  integer $seconds
     * @return integer $secondsSinceMidnight
     */
    protected function getSecondsSinceMidnight($seconds)
    {
        return $seconds % 86400;
    }

    protected function isOpenAtThisTime()
    {
        $secondsSinceMidnight = $this->getCurrentTime()->getTimestamp() - $this->getMidnight()->getTimestamp();

        if ($secondsSinceMidnight >= $this->getSecondsSinceMidnight($this->getMorningOpenTime()) && $secondsSinceMidnight <= $this->getSecondsSinceMidnight($this->getMorningCloseTime())) {
            return true;
        }

        if ($this->getSecondsSinceMidnight($this->getAfternoonOpenTime()) === 0 || $this->getSecondsSinceMidnight($this->getAfternoonCloseTime() === 0)) {
            return false;
        }

        if ($secondsSinceMidnight >= $this->getSecondsSinceMidnight($this->getAfternoonOpenTime()) && $secondsSinceMidnight <= $this->getSecondsSinceMidnight($this->getAfternoonCloseTime())) {
            return true;
        }

        return false;
    }

    protected function isOpenThisDayOfWeek()
    {
        return ($this->getDays() & pow(2, $this->getDayOfWeek())) !== 0;
    }

    protected function isOpenAtThisDate()
    {
        return $this->getMidnight() == $this->getMidnightExtraDay();
    }
}
