<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

return array(
    'ctrl' => array(
        'title'    => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'versioningWS' => 2,
        'versioning_followPages' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'opening_hours,national_holidays,regional_holidays,custom_holidays,hours,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('store_hours').'Resources/Public/Icons/tx_storehours_domain_model_store.png',
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, opening_hours, national_holidays, regional_holidays, custom_holidays, hours',
    ),
    'types' => array(
        '1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, opening_hours;;;richtext:rte_transform[mode=ts_links], hours, --div--;LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours.tabs.store.0, national_holidays, regional_holidays, custom_holidays, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0),
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_storehours_domain_model_store',
                'foreign_table_where' => 'AND tx_storehours_domain_model_store.pid=###CURRENT_PID### AND tx_storehours_domain_model_store.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ),
        ),

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),

        'title' => array(
          'exclude' => 0,
          'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.title',
          'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim,required',
          ) ,
        ) ,
        'opening_hours' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.opening_hours',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim,required',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords' => 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script',
                    ),
                ),
            ),
        ),
        'national_holidays' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays',
            'config' => array(
                'type' => 'check',
                'items' => array(
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.0',
                    0,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.1',
                    1,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.2',
                    2,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.3',
                    3,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.4',
                    4,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.5',
                    5,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.6',
                    6,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.7',
                    7,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.national_holidays.I.8',
                    8,
                  ) ,
                ) ,
                'size' => 10,
                'eval' => 'required',
                'default' => 511,
            ),
        ),
        'regional_holidays' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays',
            'config' => array(
                'type' => 'check',
                'items' => array(
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.0',
                    0,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.1',
                    1,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.2',
                    2,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.3',
                    3,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.4',
                    4,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.5',
                    5,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.6',
                    6,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.7',
                    7,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.8',
                    8,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.regional_holidays.I.9',
                    9,
                  ) ,
                ),
                'size' => 10,
                'eval' => 'required',
                'default' => 272,
            ),
        ),
        'custom_holidays' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.custom_holidays',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ),
        ),
        'hours' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_store.hours',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_storehours_domain_model_hour',
                'foreign_field' => 'store',
                'maxitems'      => 9999,
                'appearance' => array(
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                ),
            ),

        ),

    ),
);
