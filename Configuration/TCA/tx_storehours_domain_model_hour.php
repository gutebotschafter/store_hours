<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

return array(
    'ctrl' => array(
        'title'    => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour',
        'label' => 'days',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'requestUpdate' => 'regular_hours',
        'hideTable' => true,

        'versioningWS' => 2,
        'versioning_followPages' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'regular_hours,days,extra_day,morning_open_time,morning_close_time,afternoon_open_time,afternoon_close_time,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('store_hours').'Resources/Public/Icons/tx_storehours_domain_model_hour.png',
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, regular_hours, days, extra_day, morning_open_time, morning_close_time, afternoon_open_time, afternoon_close_time',
    ),
    'types' => array(
        '1' => array('showitem' => '--palette--;LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours.palettes.store.0;opening_days, --palette--;LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours.palettes.store.1;times, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
        'opening_days' => array(
          'showitem' => 'regular_hours, days, extra_day',
          'canNotCollapse' => 1,
        ),
        'times' => array(
          'showitem' => 'morning_open_time, morning_close_time, afternoon_open_time, afternoon_close_time',
          'canNotCollapse' => 1,
        ) ,
    ),
    'columns' => array(

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0),
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_storehours_domain_model_hour',
                'foreign_table_where' => 'AND tx_storehours_domain_model_hour.pid=###CURRENT_PID### AND tx_storehours_domain_model_hour.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ),
        ),

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),

        'regular_hours' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.regular_hours',
            'config' => array(
                'type' => 'check',
                'default' => 1,
            ),
        ),
        'days' => array(
            'exclude' => 0,
            'displayCond' => 'FIELD:regular_hours:=:1',
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days',
            'config' => array(
                'type' => 'check',
                'cols' => 10,
                'items' => array(
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.0',
                    0,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.1',
                    1,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.2',
                    2,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.3',
                    3,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.4',
                    4,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.5',
                    5,
                  ) ,
                  array(
                    'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.days.I.6',
                    6,
                  ) ,
                ) ,
                'eval' => 'required',
                'default' => 31,
            ) ,
        ),
        'extra_day' => array(
            'exclude' => 0,
            'displayCond' => 'FIELD:regular_hours:=:0',
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.extra_day',
            'config' => array(
                'type' => 'input',
                'size' => 7,
                'eval' => 'date',
                'checkbox' => 1,
                'default' => '',
            ),
        ),
        'morning_open_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.morning_open_time',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'time,required',
                'checkbox' => 1,
                'default' => date_create('T09:00:00UTC')->format('U'),

            ),
        ),
        'morning_close_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.morning_close_time',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'time,required',
                'checkbox' => 1,
                'default' => date_create('T18:00:00UTC')->format('U'),
            ),
        ),
        'afternoon_open_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.afternoon_open_time',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'time',
                'checkbox' => 1,
                'default' => '',
            ),
        ),
        'afternoon_close_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:store_hours/Resources/Private/Language/locallang_db.xlf:tx_storehours_domain_model_hour.afternoon_close_time',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'time',
                'checkbox' => 1,
                'default' => '',
            ),
        ),

        'store' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
    ),
);
