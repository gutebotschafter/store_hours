<?php
namespace GuteBotschafter\StoreHours\Tests\Unit\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Morton Jonuschat <mj@gute-botschafter.de>, Gute Botschafter GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class GuteBotschafter\StoreHours\Controller\StoreController.
 *
 * @author Morton Jonuschat <mj@gute-botschafter.de>
 */
class StoreControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \GuteBotschafter\StoreHours\Controller\StoreController
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = $this->getMock('GuteBotschafter\\StoreHours\\Controller\\StoreController', array('redirect', 'forward', 'addFlashMessage'), array(), '', false);
    }

    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function listActionFetchesStoreFromRepositoryAndAssignsItToView()
    {
        $store = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', false);

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $view->expects($this->once())->method('assign')->with('store', $store);

        $storeRepository = $this->getMock('GuteBotschafter\\StoreHours\\Domain\\Repository\\StoreRepository', array('findByUid'), array(), '', false);
        $storeRepository->expects($this->once())->method('findByUid')->will($this->returnValue($store));

        $this->inject($this->subject, 'settings', array('store' => 0));
        $this->inject($this->subject, 'storeRepository', $storeRepository);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
