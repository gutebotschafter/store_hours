<?php

namespace GuteBotschafter\StoreHours\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Morton Jonuschat <mj@gute-botschafter.de>, Gute Botschafter GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \GuteBotschafter\StoreHours\Domain\Model\Hour.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Morton Jonuschat <mj@gute-botschafter.de>
 */
class HourTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \GuteBotschafter\StoreHours\Domain\Model\Hour
     */
    protected $subject = null;

    protected function setUp()
    {
    }

    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function isOpenChecksDayOfWeek()
    {
        $this->travelTo('Monday 11:23:53');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Tuesday 09:41:00');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Wednesday 17:05:00');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Thursday 10:20:30');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Friday 11:11:11');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Friday 11:11:11');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Saturday 10:00:00');
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Sunday 12:00:00');
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isOpenChecksOperatingHours()
    {
        $this->travelTo('Monday 08:00:00');
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Monday 11:00:00');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Monday 14:00:00');
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Monday 17:00:00');
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Monday 20:00:00');
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isOpenIgnoresDayOfWeekForNonRegularHours()
    {
        $this->travelTo('Saturday 11:00:00');
        $this->subject->setRegularHours(false);
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Sunday 08:00:00');
        $this->subject->setRegularHours(false);
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Sunday 11:00:00');
        $this->subject->setRegularHours(false);
        $this->assertEquals(true, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isOpenIgnoresUnsetAfternoonTimes()
    {
        $this->travelTo('Monday 08:00:00');
        $this->subject->setAfternoonOpenTime(0);
        $this->subject->setAfternoonCloseTime(0);
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('Monday 11:00:00');
        $this->subject->setAfternoonOpenTime(0);
        $this->subject->setAfternoonCloseTime(0);
        $this->assertEquals(true, $this->subject->isOpen());

        $this->travelTo('Monday 14:00:00');
        $this->subject->setAfternoonOpenTime(0);
        $this->subject->setAfternoonCloseTime(0);
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * Stub the internal date helper to the given date
     *
     * @param string $date
     */
    protected function travelTo($date = '2000-01-01 05:23:42')
    {
        $this->subject = $this->getMockBuilder('GuteBotschafter\\StoreHours\\Domain\\Model\\Hour')
          ->setMethods(array('getCurrentTime'))
          ->getMock();
        $this->subject->method('getCurrentTime')->willReturn(new \DateTimeImmutable($date));
        $this->subject->setExtraDay(new \DateTime('Sunday'));
        $this->subject->setDays(31);                        # Mon-Fri
        $this->subject->setMorningOpenTime(9 * 3600);       # 09:00
        $this->subject->setMorningCloseTime(12.5 * 3600);   # 12:30
        $this->subject->setAfternoonOpenTime(14.5 * 3600);  # 14:30
        $this->subject->setAfternoonCloseTime(18 * 3600);   # 18:00
    }
}
