<?php

namespace GuteBotschafter\StoreHours\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Morton Jonuschat <mj@gute-botschafter.de>, Gute Botschafter GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \GuteBotschafter\StoreHours\Domain\Model\Store.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Morton Jonuschat <mj@gute-botschafter.de>
 */
class StoreTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \GuteBotschafter\StoreHours\Domain\Model\Store
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = new \GuteBotschafter\StoreHours\Domain\Model\Store();
    }

    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function isClosedOnNationalHoliday()
    {
        # New Year's Day
      $this->travelTo('2015-01-01');
        $this->subject->setNationalHolidays(1);
        $this->assertEquals(false, $this->subject->isOpen());

      # Good Friday
      $this->travelTo('2015-04-03');
        $this->subject->setNationalHolidays(2);
        $this->assertEquals(false, $this->subject->isOpen());

      # Easter Monday
      $this->travelTo('2015-04-06');
        $this->subject->setNationalHolidays(4);
        $this->assertEquals(false, $this->subject->isOpen());

      # Labor Day
      $this->travelTo('2015-05-01');
        $this->subject->setNationalHolidays(8);
        $this->assertEquals(false, $this->subject->isOpen());

      # Ascension Day
      $this->travelTo('2015-05-14');
        $this->subject->setNationalHolidays(16);
        $this->assertEquals(false, $this->subject->isOpen());

      # Whitmonday
      $this->travelTo('2015-05-25');
        $this->subject->setNationalHolidays(32);
        $this->assertEquals(false, $this->subject->isOpen());

      # German Unification Day
      $this->travelTo('2015-10-03');
        $this->subject->setNationalHolidays(64);
        $this->assertEquals(false, $this->subject->isOpen());

      # Christmas Day
      $this->travelTo('2015-12-25');
        $this->subject->setNationalHolidays(128);
        $this->assertEquals(false, $this->subject->isOpen());

      # Day after Christmas Day
      $this->travelTo('2015-12-26');
        $this->subject->setNationalHolidays(256);
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isClosedOnRegionalHoliday()
    {

      # Epiphany
      $this->travelTo('2015-01-06');
        $this->subject->setRegionalHolidays(1);
        $this->assertEquals(false, $this->subject->isOpen());

      # Holy Thursday
      $this->travelTo('2015-04-02');
        $this->subject->setRegionalHolidays(2);
        $this->assertEquals(false, $this->subject->isOpen());

      # Easter Sunday
      $this->travelTo('2015-04-05');
        $this->subject->setRegionalHolidays(4);
        $this->assertEquals(false, $this->subject->isOpen());

      # Whitsunday
      $this->travelTo('2015-05-24');
        $this->subject->setRegionalHolidays(8);
        $this->assertEquals(false, $this->subject->isOpen());

      # Corpus Christi
      $this->travelTo('2015-06-04');
        $this->subject->setRegionalHolidays(16);
        $this->assertEquals(false, $this->subject->isOpen());

      # Augsburger Friedensfest
      $this->travelTo('2015-08-08');
        $this->subject->setRegionalHolidays(32);
        $this->assertEquals(false, $this->subject->isOpen());

      # Assumption Day
      $this->travelTo('2015-08-15');
        $this->subject->setRegionalHolidays(64);
        $this->assertEquals(false, $this->subject->isOpen());

      # Reformation Day
      $this->travelTo('2015-10-31');
        $this->subject->setRegionalHolidays(128);
        $this->assertEquals(false, $this->subject->isOpen());

      # All Hallows
      $this->travelTo('2015-11-01');
        $this->subject->setRegionalHolidays(256);
        $this->assertEquals(false, $this->subject->isOpen());

      # Day of Prayer and Repentance
      $this->travelTo('2015-11-17');
        $this->subject->setRegionalHolidays(512);
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isClosedOnCustomHoliday()
    {
        $this->travelTo('2015-01-02');
        $this->subject->setCustomHolidays("2015-01-02\n2015-01-03");
        $this->assertEquals(false, $this->subject->isOpen());

        $this->travelTo('2015-01-03');
        $this->subject->setCustomHolidays("2015-01-02\n2015-01-03");
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isOpenOnRegularHours()
    {
        $this->travelTo('2015-01-04');
        $this->subject->setNationalHolidays(511);
        $this->subject->addHour($this->getOpenRegularHour());
        $this->assertEquals(true, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isOpenOnSpecialDates()
    {
        $this->travelTo('2015-01-01');
        $this->subject->setNationalHolidays(511);
        $this->subject->addHour($this->getOpenExtraHour('2015-01-01'));
        $this->assertEquals(true, $this->subject->isOpen());
    }

    /**
     * @test
     */
    public function isClosedBySpecialDates()
    {
        $this->travelTo('2015-01-14');
        $this->subject->setNationalHolidays(511);
        $this->subject->addHour($this->getOpenRegularHour());
        $this->subject->addHour($this->getClosedExtraHour('2015-01-14'));
        $this->assertEquals(false, $this->subject->isOpen());
    }

    /**
     * Stub the internal date helpers to the given date
     *
     * @param string $date
     */
    protected function travelTo($date = '2000-01-01')
    {
        $this->subject = $this->getMockBuilder('GuteBotschafter\\StoreHours\\Domain\\Model\\Store')
        ->setMethods(array('getToday'))
        ->getMock();
        $this->subject->method('getToday')->willReturn(new \DateTime($date));
    }

    /**
     * Mock extra hour that always returns open
     *
     * @param string $date
     * @return \GuteBotschafter\StoreHours\Domain\Model\Hour
     */
    protected function getOpenExtraHour($date = '2000-01-01')
    {
        $extraHour = $this->getMockBuilder('GuteBotschafter\\StoreHours\\Domain\\Model\\Hour')
        ->setMethods(array('isOpen', 'getExtraDay'))
        ->getMock();
        $extraHour->method('getExtraDay')->willReturn(new \DateTime($date));
        $extraHour->method('isOpen')->willReturn(true);
        $extraHour->setRegularHours(false);

        return $extraHour;
    }

    /**
     * Mock extra hour that always returns closed
     *
     * @param string $date
     * @return \GuteBotschafter\StoreHours\Domain\Model\Hour
     */
    protected function getClosedExtraHour($date = '2000-01-01')
    {
        $extraHour = $this->getMockBuilder('GuteBotschafter\\StoreHours\\Domain\\Model\\Hour')
        ->setMethods(array('isOpen', 'getExtraDay'))
        ->getMock();
        $extraHour->method('getExtraDay')->willReturn(new \DateTime($date));
        $extraHour->method('isOpen')->willReturn(false);
        $extraHour->setRegularHours(false);

        return $extraHour;
    }

    /**
     * Mock extra hour that always returns open
     *
     * @return \GuteBotschafter\StoreHours\Domain\Model\Hour
     */
    protected function getOpenRegularHour()
    {
        $extraHour = $this->getMockBuilder('GuteBotschafter\\StoreHours\\Domain\\Model\\Hour')
        ->setMethods(array('isOpen'))
        ->getMock();
        $extraHour->method('isOpen')->willReturn(true);
        $extraHour->setRegularHours(true);

        return $extraHour;
    }
}
