<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'GuteBotschafter.'.$_EXTKEY,
    'Main',
    array(
        'Store' => 'list',

    ),
    // non-cacheable actions
    array(
        'Store' => 'list',

    )
);
