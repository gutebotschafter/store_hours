﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================

.. _admin-installation:

Installation
------------

To install the extension, perform the following steps:

#. Go to the Extension Manager
#. Install the extension
#. Load the static template

For a list of configuration options, using a definition list is recommended:

.. _admin-configuration:

Configuration
-------------

The static template does not offer any configuration options beyond the Extbase template
paths. Please configure the paths using the TYPO3 constant editor.
