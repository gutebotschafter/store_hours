﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _user-manual:

Anleitung
=========

Um die Öffnungszeiten auf der Webseite auszugeben muss zuallerest ein Datensatz vom Typ „Ladenlokal”
angelegt werden. Dieser Datensatz enthält die Öffnungszeiten als Freitext um eine „ansprechende” Formulierung
der Öffnungszeiten zu ermöglichen. Zusätzlich können Regeln für die Öffnungzeiten hinterlegt werden um
der Erweiterung die Ausgabe zu ermöglichen ob ein Ladenlokal gerade geöffnet hat.

Reguläre Öffnungszeiten
-----------------------

Reguläre Öffnungszeiten legen die Öffnungszeiten fest die gelten wenn kein Feiertag oder Sonderverkaufstag ist.
Wenn Sie ein Ladenlokal haben welches von Montag bin Freitag von 09:00 bis 13:00 und 14:00 bis 17:00 Uhr geöffnet
ist, außer Mittwochs am Nachmittag, gehen Sie wie folgt vor:
das Mittwochs

Öffnungszeiten für Montag, Dienstag, Donnerstag und Freitag
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

* Der Haken bei „Reguläre Öffnungszeit” muss gesetzt sein
* Bei den Wochentagen sollten Mo, Di, Do und Fr. markiert sein
* Der erste Block Öffnungszeiten enthält 09:00 und 13:00
* Der zweite Block Öffnungszeiten enthält 14:00 und 17:00

Öffnungszeiten für Mittwoch
"""""""""""""""""""""""""""

* Der Haken bei „Reguläre Öffnungszeit” muss gesetzt sein
* Bei den Wochentagen sollten Mittwoch markiert sein
* Der erste Block Öffnungszeiten enthält 09:00 und 13:00
* Der zweite Block Öffnungszeiten bleibt leer / 00:00

Sonderverkaufstage
------------------

Sonderverkaufstage können zu der Liste hinzugefügt werden. In diesem Fall darf der Haken bei „Reguläre Öffnungszeit”
nicht gesetzt werden, stattdessen muss ein Datum für den Sonderverkauf ausgewählt werden. Die Öffnungszeiten werden
wie bisher definiert.

Feiertage
---------

Der Reiter „Feiertage” erlaubt die Auswahl von gesetzlichen Feiertagen an denen das Ladenlokal geschlossen bleibt.

.. tip::

   Zusätzliche Tage an denen das Ladenlokal nicht geöffnet wird können im Feld „Betriebferien”
   eingetragen werden. Die Datumsangaben erfolgen im Format JJJJ-MM-TT, also zum Beispiel
   2015-05-31 damit am letzen Tag des Mai 2015 nicht geöffnet ist. Mehre Termine können angegeben werden,
   einer pro Zeile.
