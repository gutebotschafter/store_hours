﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _start:

=============================================================
Store Hours (Deutsch)
=============================================================

.. only:: html

	:Klassifikation:
		store_hours

	:Version:
		|release|

	:Sprache:
		de

	:Beschreibung:
		Ausgabe von Öffnungszeiten von Ladenlokalen auf der Webseite. Erlaubt die flexible Konfiguration von Feier- und Sonderverkaufstagen.

	:Schlüsselwörter:
		öffnungszeiten,laden,ladenlokal,geschäft

	:Copyright:
		2015

	:Autor:
		Morton Jonuschat, Gute Botschafter GmbH

	:E-Mail:
		mj@gute-botschafter.de

	:Lizenz:
		Dieses Dokument wird unter der Open Content License, siehe
		http://www.opencontent.org/opl.shtml veröffentlicht.

	:Gerendert:
		|today|

	Der Inhalt dieses Dokuments bezieht sich auf TYPO3,
	ein GNU/GPL CMS-Framework auf `www.typo3.org <http://www.typo3.org/>`_.


	**Inhaltsverzeichnis**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	ChangeLog/Index
	Targets
