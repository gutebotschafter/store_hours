﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Anleitung für den Administrator
===============================

.. _admin-installation:

Installation
------------

Um die Erweiterung zu installieren sind die folgenden Schritte erforderlich:

#. Öffnen Sie den Erweiterungs-Manager
#. Installieren Sie die Erweiterung
#. Laden Sie das statische TypoScript Template.

.. _admin-configuration:

Konfiguration
-------------

Abgesehen von der Konfiguration der Extbase HTML Template Pfade bietet das TypoScript-Template
keine weiteren Konfigurationsoptionen.
