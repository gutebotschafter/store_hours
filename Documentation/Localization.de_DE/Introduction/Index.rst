.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Einleitung
============


.. _what-it-does:

Funktion / Aufgabe
------------------

Ausgabe von Öffnungszeiten von Ladenlokalen auf der Webseite. Die Erweiterung ermöglicht die flexible Konfiguration von Feiertagen,
Betriebsferien und Sonderverkaufstagen. Zusätzlich kann über ein Rich-Text Editor auch ein zusätzlicher Freitext ausgegeben werden.
Die Regelbasierte Definition der Öffnungszeiten erlaubt es der Erweiterung einen Meldung auf der Webseite auszugeben ob das Lokal
gerade geöffnet oder geschlossen ist.
