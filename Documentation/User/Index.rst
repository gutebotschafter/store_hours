﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _user-manual:

Users Manual
============

To show the operating hours of a shop on your website you have to create a „store” record first.
This record contains a rich text field that allows you to show your operating hours in a „human”
readable form, additionally you can define operating hours that help the frontend plugin to determine
wether the store is currently open. When you include the frontend plugin on a page you can select
this record in the plugin configuration.

Regular operating hours
-----------------------

Regular operating hours define the default store hours. You can have multiple entries of this type to
allow for different operating hours on days. Given that your store is open from 9 to 5 from monday to
friday with a lunch break from 1 to 2pm with an exception for wednesdays where you already close at 1.

Operating hours for monday, tuesday, thursday and friday
""""""""""""""""""""""""""""""""""""""""""""""""""""""""

* Make sure the checkbox „Regular hours” is activated
* Activate the check the boxes for monday, tuesday, thursday and friday
* Add 9:00am as the time when you open in the morning and 1:00pm as the time when you close
* Add 2:00pm as the time when you open in the afternoon and 5:00pm as the time when you close.

Operating hours for wednesday
"""""""""""""""""""""""""""""

* Make sure the checkbox „Regular hours” is activated
* Activate only the check the box for wednesday
* Add 9:00am as the time when you open in the morning and 1:00pm as the time when you close
* Leave the times for the afternoon empty.

Special events
--------------

You can add entries for dates when there are special operating hours. To do that you need to add
an entry to the opening hours that has the „Regular hours” box unchecked. You can then pick the
date when these hours will be in effect. Add the times when the store is operating as usual. Entries
of this type will override all other configured times including holidays.

Holidays
--------

The tab „Holidays” allows you to configure holidays when the normal operating hours are not in effect.
You can select from a preconfigured list of german national holidays. Just tick the boxes for the days
when your store will be closed.

.. tip::

   You can configure custom dates when your store is closed. Enter these into the text field
   „Custom holidays”. Format the dates as YYYY-MM-DD, for example 2015-05-31 to have your store
   closed on the last day of march 2015. You can enter multiple dates, one per line.
