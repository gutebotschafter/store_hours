﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Store Hours
=============================================================

.. only:: html

	:Classification:
		store_hours

	:Version:
		|release|

	:Language:
		en

	:Description:
		Output business hours of a shop on the web page. Allows for flexible configuration of holidays and special events.

	:Keywords:
		store,shop,business hours

	:Copyright:
		2015

	:Author:
		Morton Jonuschat, Gute Botschafter GmbH

	:Email:
		mj@gute-botschafter.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	ChangeLog/Index
	Targets
