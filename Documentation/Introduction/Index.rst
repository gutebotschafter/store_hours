﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

Output business hours of a shop on the web page. Allows for flexible configuration of holidays and special events. You can
enter a custom message that will be displayed, additionally a message can be shown wether the store is currently open or closed.
Lot's of exceptions to the regular operating hours may be defined in the plugin.
